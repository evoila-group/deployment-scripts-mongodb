#!/bin/bash

export REPOSITORY_MONGO="https://bitbucket.org/meshstack/deployment-scripts-mongodb/raw/HEAD/mongodb"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-monit/raw/HEAD/monit"

wget $REPOSITORY_MONGO/mongo-template.sh --no-cache
chmod +x mongo-template.sh
./mongo-template.sh -d evoila -u evoila -v evoila -p evoila -e openstack
