#!/bin/bash
echo
echo "### Starting configuration of MongoDB ... ###"
echo

export LC_ALL=C

#starts with parameters for dbname, user and password set in mongo-template.sh
echo "mongo_db = ${MONGO_DB}"
echo "mongo_user = ${MONGO_USER_ADMIN}"
echo "environment = ${ENVIRONMENT}"


#creates config-file

#folder for config-file
mkdir -p /etc/mongodb/
#folder for pid-file
mkdir -p /var/run/mongodb/
chown -R mongodb:mongodb /var/run/mongodb/

echo "
storage:
  dbPath: \"${MONGO_DATA_PATH}\"
  directoryPerDB: true
  journal:
    enabled: true
systemLog:
   destination: file
   path: "/var/log/mongodb/mongod.log"
   logAppend: true
security:
   authorization: "disabled"
net:
   port: 27017" > $MONGO_CONF

# for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
 echo "processManagement:
     pidFilePath: "/var/run/mongodb/mongod.pid"" >> $MONGO_CONF
 $OPENSTACK_START
fi
# for docker as environment
if [ "$ENVIRONMENT" = 'docker' ]; then
 $DOCKER_START &
fi

echo "Waiting for the mongo server to start for 90 seconds..."

sleep 90


echo "Executing default user setup"

echo "use admin
db.createUser(
  {
    user: \"$MONGO_USER_ADMIN\",
    pwd: \"$MONGO_PASSWORD\",
    roles: [ 
      { role: \"userAdminAnyDatabase\", db: \"admin\" }, 
      { role: \"dbAdminAnyDatabase\", db: \"admin\" }, 
      { role: \"readWriteAnyDatabase\", db: \"admin\" }
    ]
  }
)" > install-admin.js


#creates user for Service Broker
mongo < install-admin.js

rm install-admin.js

echo "use $MONGO_DB
db.createCollection(\"_auth\")
db._auth.insert(
   {
    auth: \"auth\"
   }
)
db._auth.drop()" > create-sb-db.js

mongo < create-sb-db.js

rm create-sb-db.js

echo "use $MONGO_DB
db.createUser(
  {
    user: \"$MONGO_USER_SERVICEBROKER\",
    pwd: \"$MONGO_PASSWORD\",
    roles: [ { role: \"readWrite\", db: \"$MONGO_DB\" } ]
  }
)
db.createUser(
  {
    user: \"$MONGO_USER_ADMIN\",
    pwd: \"$MONGO_PASSWORD\",
    roles: [ 
      { role: \"userAdmin\", db: \"$MONGO_DB\" }, 
      { role: \"dbAdmin\", db: \"$MONGO_DB\" }, 
      { role: \"readWrite\", db: \"$MONGO_DB\" }
    ]
  }
)" > install-sb-user.js

#creates user for service broker for mongod
mongo < install-sb-user.js

rm install-sb-user.js

# for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
 $OPENSTACK_STOP
fi
# for docker as environment
if [ "$ENVIRONMENT" = 'docker' ]; then
 $DOCKER_STOP &
fi

echo "Waiting for the mongo server to stop for 15 seconds..."

sleep 15


echo "
storage:
  dbPath: \"${MONGO_DATA_PATH}\"
  directoryPerDB: true
  journal:
    enabled: true
systemLog:
   destination: file
   path: \"/var/log/mongodb/mongod.log\"
   logAppend: true
security:
   authorization: \"enabled\"
net:
   port: 27017" > $MONGO_CONF

# additional config for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
  echo "processManagement:
      pidFilePath: \"/var/run/mongodb/mongod.pid\"" >> $MONGO_CONF
fi

cd /etc/systemd/system/
echo "[Unit]" > mongodb-exporter.service
echo "Description=MySQL/MariaDB exporter" >> mongodb-exporter.service
echo "After=local-fs.target network-online.target network.target" >> mongodb-exporter.service
echo "Wants=local-fs.target network-online.target network.target" >> mongodb-exporter.service
echo "" >> mongodb-exporter.service
echo "[Service]" >> mongodb-exporter.service
echo "Environment=MONGO_URI=mongodb://$ADMIN_USER:$ADMIN_PASSWORD@localhost/$ADMIN_DB" >> mongodb-exporter.service
echo "ExecStart=/opt/mongodb_exporter -mongodb.uri=$MONGO_URI" >> mongodb-exporter.service
echo "Type=simple" >> mongodb-exporter.service

chmod 755 /etc/systemd/system/mongodb-exporter.service
systemctl daemon-reload
systemctl enable mongodb-exporter.service
systemctl daemon-reload
systemctl start mongodb-exporter