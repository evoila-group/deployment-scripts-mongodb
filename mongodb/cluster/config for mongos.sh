

monit unmonitor mongodb
monit summary
# check connection to other replicaset members



mkdir -p /data/mongodb-key
echo "$KEY" >> /data/mongodb-key/key-$REPSET_NAME
chmod 600 /data/mongodb-key/key-$REPSET_NAME


# adds clusterAdmin rights to mongodb-user
echo "Adding clusterAdmin rights to mongodb-user"

echo "use admin
db.auth(\"evoila\",\"evoila\")
db.grantRolesToUser(
    \"evoila\",
    [
      { role: \"root\", db: \"admin\" },
      { role: \"clusterAdmin\", db: \"admin\" }
    ]
)" > grant-roles.js
mongo < grant-roles.js




mongos --configdb 192.168.1.203:27019,192.168.1.204:27019,192.168.1.205:27019 &





# enables using key for autorization
echo "
storage:
  dbPath: \"${MONGO_DATA_PATH}\"
  directoryPerDB: true
  journal:
    enabled: true
sharding:
   configDB: \"data\"mongodb\"configdb\"
systemLog:
   destination: file
   path: "/var/log/mongodb/mongod.log"
   logAppend: true
security:
   authorization: "enabled"
   keyFile: /data/mongodb-key/key-$REPSET_NAME
net:
   port: 27019" > $MONGOS_CONF
