#!/bin/bash

# This script only works with pre-installed and configured mongodb

export REPOSITORY_MONGO_HA="https://bitbucket.org/meshstack/deployment-scripts-mongodb/raw/HEAD/mongodb/cluster"

export REPSET_NAME="shared_cluster_1"


wget $REPOSITORY_MONGO_HA/mongo-HA-repset-template.sh --no-cache
chmod +x mongo-HA-repset-template.sh
./mongo-HA-repset-template.sh -d evoila -u evoila -p evoila -r $REPSET_NAME -k $MONGODB_KEY  -m 192.168.1.3 -s "192.168.1.4,192.168.1.5" -a primary



###################### Secondary Configuration ######################



export REPOSITORY_MONGO_HA="https://bitbucket.org/meshstack/deployment-scripts-mongodb/raw/HEAD/mongodb/cluster"

export REPSET_NAME="shared_cluster_1"


wget $REPOSITORY_MONGO_HA/mongo-HA-repset-template.sh --no-cache
chmod +x mongo-HA-repset-template.sh
./mongo-HA-repset-template.sh -d evoila -u evoila -p evoila -r $REPSET_NAME -k $MONGODB_KEY  -a secondary
