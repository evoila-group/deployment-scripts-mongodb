#!/bin/bash

export LC_ALL=C

export MONGO_MAJOR=3.4
export MONGO_VERSION=3.4.1
export MONGO_PACKAGE=mongodb-org

export MONGO_DATA_PATH=/data/mongodb/db
export MONGO_CONF=/etc/mongod.conf

#path used to check if service is installed
export CHECK_PATH=/data/mongodb

# start/stop commands for different environments
export OPENSTACK_START="systemctl start mongod"
export OPENSTACK_STOP="systemctl stop mongod"
export DOCKER_START="mongod --config ${MONGO_CONF} --smallfiles"
export DOCKER_STOP="mongod --config ${MONGO_CONF}  --shutdown"

#configuration for logging
export LOG_HOST="172.24.102.12"
export LOG_PORT="5002"
export LOG_SENDING_PROTOCOL="tcp"


#parameters for dbname, user and password used in mongo-entrypoint.sh
usage() { echo "Usage: $0 [-d <string>] [-u <string>] [-v <string> optional] [-p <string>] [-e <string>]" 1>&2; exit 1; }

while getopts ":d:u:v:p:e:" o; do
    case "${o}" in
        d)
            MONGO_DB=${OPTARG}
            ;;
        p)
            MONGO_PASSWORD=${OPTARG}
            ;;
        u)
            MONGO_USER_ADMIN=${OPTARG}
            ;;
        v)
            MONGO_USER_SERVICEBROKER=${OPTARG}
            ;;
        e)
            ENVIRONMENT=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${MONGO_DB}" ] || [ -z "${MONGO_USER_ADMIN}" ] || [ -z "${MONGO_PASSWORD}" ] || [ -z "${ENVIRONMENT}" ]; then
    usage
fi

# export for following scripts
export MONGO_DB=${MONGO_DB}
export MONGO_USER_ADMIN=${MONGO_USER_ADMIN}
export MONGO_USER_SERVICEBROKER=${MONGO_USER_SERVICEBROKER}
export MONGO_PASSWORD=${MONGO_PASSWORD}
export ENVIRONMENT=${ENVIRONMENT}

echo "mongo_db = ${MONGO_DB}"
echo "mongo_user_admin = ${MONGO_USER_ADMIN}"
echo "mongo_user_servicebroker = ${MONGO_USER_SERVICEBROKER}"
echo "environment = ${ENVIRONMENT}"
echo "repository_mongo = ${REPOSITORY_MONGO}"
echo "check_path = ${CHECK_PATH}"

# checks if service is installed
if [ -a $CHECK_PATH* ]; then
    # executes script for startup of mongodb
    chmod +x mongo-run.sh
    ./mongo-run.sh mongod
else
    if [ "$ENVIRONMENT" = 'openstack' ]; then
      # loads and executes script for logging to a (central) logging instance
      wget $REPOSITORY_MONGO/mongo-logging.sh
      chmod +x mongo-logging.sh
      ./mongo-logging.sh
    fi
    # loads and executes script for automatic installation of mongodb
    wget $REPOSITORY_MONGO/mongo-install.sh
    chmod +x mongo-install.sh
    ./mongo-install.sh

    # loads includes for the monit controlfile for this database
    mkdir -p /etc/monit.d/
    wget $REPOSITORY_MONGO/mongo-monitrc -P /etc/monit.d/

    # loads and executes script for configuration of mongodb
    wget $REPOSITORY_MONGO/mongo-configuration.sh
    chmod +x mongo-configuration.sh
    ./mongo-configuration.sh

    # loads and executes script for startup of mongodb
    wget $REPOSITORY_MONGO/mongo-run.sh
    chmod +x mongo-run.sh
    ./mongo-run.sh mongod
fi

# installs monit for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
  export REPOSITORY_MONIT="${REPOSITORY_MONIT}"
  wget $REPOSITORY_MONIT/monit-template.sh
  chmod +x monit-template.sh
  ./monit-template.sh -u monit -p ${MONGO_PASSWORD}
fi
